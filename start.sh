#!/bin/bash

###############CONFIGURATION#################################
#Server Folder
DIR="/My/Server/Path/"

#Start command line
START="java -Dfile.encoding=utf-8 -Dcom.mojang.eula.agree=true -Xmx16G -XX:MaxPermSize=128M -jar spigot.jar --log-strip-color"

#TMUX Session name (the same name than what you defined in the "NAME" variable of your server /etc/init.d/mc-server-name)
NAME="tmux-session-name"
###############END CONFIGURATION#############################

tmux send -t ${NAME} "cd $DIR" ENTER
sleep 1
tmux send -t ${NAME} "$START" ENTER
exit